// Toggle Nav
(function() {
    var toggle = document.getElementById('toggle-nav');
    var navbar = document.querySelector('.navbar');
    
    toggle.addEventListener('click', togglenav);
    
    function togglenav() {
        navbar.classList.toggle('active');
    }
})();

// Form validation
(function() {

    // retrive input values
    var firstName = document.getElementById('first-name');
    var lastName = document.getElementById('last-name');
    var email = document.getElementById('email');
    var country = document.getElementById('country');
    var interest = document.getElementsByName('checkbox');
    var overage = document.getElementById('overage');
    var privacy = document.getElementById('privacy');
    
    // retrive error containers
    var firstNameErr = document.getElementById('first-name_error');
    var lastNameErr = document.getElementById('last-name_error');
    var emailErr = document.getElementById('email_error');
    var countryErr = document.getElementById('country_error');
    var interestErr = document.getElementById('interest_error');
    var overageErr = document.getElementById('overage_error');
    var privacyErr = document.getElementById('privacy_error');

    // retrive modal components
    var modal = document.getElementById('modal');
    var modalText = document.getElementById('modal-text');
    var modalClose = document.querySelector('.close');

    // interest list 
    var interestList;
    
    // invoke form onsubmit
    document.getElementById('form-container').onsubmit = function(e) {
        e.preventDefault();

        var submit = true;
        var isChecked = false;

        var errorsArr = [
            "Please enter your first name",
            "Please enter your last name", 
            "Please enter a valid email address",
            "Please enter country of redidence",
            "Please select at least one topic",
            "Please confirm you are over 16",
            "Please agree to the terms of the Privacy Policy"
        ];

        // empty array
        interestList = [];

        if(firstName.value == null || firstName.value == "") {
            firstNameErr.innerHTML = errorsArr[0];
            firstName.classList.add('has-error');
            submit = false;
        }

        if(lastName.value == null || lastName.value == "") {
            lastNameErr.innerHTML = errorsArr[1];
            lastName.classList.add('has-error');
            submit = false;
        }

        if(email.value == null || email.value == "") {
            emailErr.innerHTML = errorsArr[2];
            email.classList.add('has-error');
            submit = false;
        }

        if(country.value == null || country.value == "") {
            countryErr.innerHTML = errorsArr[3];
            country.classList.add('has-error');
            submit = false;
        }

        for (var i = 0; i < interest.length; i++) {
            if(interest[i].checked) {
                isChecked = true;
                console.log(interest[i].value);
            }
            
        }        

        if(!isChecked) {
            interestErr.innerHTML = errorsArr[4];
            submit = false;
        }

        if(!overage.checked) {
            overageErr.innerHTML = errorsArr[5];
            submit = false;
        }

        if(!privacy.checked) {
            privacyErr.innerHTML = errorsArr[6];
            submit = false;
        }

        if(submit) {
            showModal();
        }

    }

    // Close modal 
    modalClose.addEventListener('click', closeModal);

    // function
    function showModal() {
        modal.style.display = 'block';
        modalText.innerHTML = "Thank you " + firstName.value + " " + lastName.value + " from " + country.value + ". You'll be recieving our newsletters about " + "[interest list]" + " in your " + email.value +" email.";
    }

    function closeModal() {
        modal.style.display = 'none';
    }  


    function removeErr() {
        document.getElementById(this.id + "_error").innerHTML = "";
        document.getElementById(this.id).classList.remove("has-error");   
    }
    
    // Remove errors from DOM 
    firstName.onkeyup = removeErr;
    lastName.onkeyup = removeErr;
    email.onkeyup = removeErr;
    country.onkeyup = removeErr;

})();