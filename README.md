# DK Publisher  

Develop a single page form with the use of a CSS framework and JavaScript.

Frontend SASS Boilerplate used - Primitive.

All documentation for Primitive can be found at [https://taniarascia.github.io/primitive]

## Getting Started
- clone repo [https://kchavda1984@bitbucket.org/kchavda1984/dkpublisher-form.git]
- cd dkpublisher-form
- npm install
- gulp